import React from 'react'
import { Typography,Container,Box,Grid,Button } from '@material-ui/core'
import Nav from './comp/Nav'
import { makeStyles } from '@material-ui/core/styles';
import { BrowserRouter, Route, Link, Switch } from "react-router-dom";
import Head2 from './comp/Head2'
import Head3 from './comp/Head3'
import image from "./banner-img.jpg"
import Cards from './comp/Cards'
import Cards2 from './comp/Cards2'
import Cards3 from './comp/Cards3'
import Cards4 from './comp/Cards4'
import Cards5 from './comp/Cards5'
import Cards6 from './comp/Cards6'
import Cards7 from './comp/Cards7'
import Cards8 from './comp/Cards8'
import Footer from './comp/Footer'

import Menus from './comp/Menus';
const useStyles=  makeStyles((theme)=> ({
  root: {
   backgroundColor:"#fff"
  },
  hero:{
    height: "500px",
    backgroundPosition: "center",
    backgroundRepeat:"no-repeat",
    backgroundSize:"cover",
     position:"relative",
    display:"flex",
    
    alignItems:"center",
    color:"#fff",
    fontSize:"4rem",
    [theme.breakpoints.down("sm")]:{
      height:300,
      fontSize:"2em"
    }
},
 

}));


function App() {
  const classes=useStyles();
  return (

    <div className="App">
 
<Nav />
<Box style={{backgroundImage:`url(${image})`}} className={classes.hero}>
        <Box ml={12}> 
         
         <Typography variant="h2"> Welcome to Hasty and Nasty <br/> Restaurent</Typography>
         <Box mt={4}>  <Button variant="outlined" style={{color:"white"}}> Check Out Our Menu </Button> </Box>

        </Box>
      </Box>
<Container >
     <Head2 />
     <Grid container spacing={4}>
  <Grid item xs={12} sm={6} md={3}>  <Cards /> </Grid>
  <Grid item xs={12} sm={6} md={3}>  <Cards2 /> </Grid>
  <Grid item xs={12} sm={6} md={3}>  <Cards3 /> </Grid>
  <Grid item xs={12} sm={6} md={3}>  <Cards4 /> </Grid>


     </Grid>
    
   


  </Container>
   <Box mt={4}>
   <Head3 /> </Box>

   <Menus />


   <Container>
<Box ml={4} mt={4} pb={4}>

   <Grid container spacing={4}>
  <Grid item xs={12} sm={6} md={3}>  <Cards5 /> </Grid>
  <Grid item xs={12} sm={6} md={3}>  <Cards6 /> </Grid>
  <Grid item xs={12} sm={6} md={3}>  <Cards7 /> </Grid>
  <Grid item xs={12} sm={6} md={3}>  <Cards8 /> </Grid>


     </Grid>
     </Box>
    
   </Container>
  
   <Footer />

  </div>  
    
  );
}

export default App;
