import React from 'react'
import { Typography,Container,Box,Grid,Button } from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles';
import { BrowserRouter, Route, Link, Switch } from "react-router-dom";
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import InstagramIcon from '@material-ui/icons/Instagram';

import image from "../bg.png"
import image1 from "../blog-img-1.jpg"
const useStyles=  makeStyles((theme)=> ({
  root: {
   backgroundColor:"#fff"
  },
  hero:{
    height: "200px",
    backgroundPosition: "center",
    backgroundRepeat:"no-repeat",
    backgroundSize:"cover",
     position:"relative",
    displa:"flex",
    justifyContent:"center",
    alignItems:"center",
    color:"#fff",
    // fontSize:"4rem",
    [theme.breakpoints.down("sm")]:{
      height:300,
      fontSize:"2em"
    }
},
 img:{
     height:100,
     backgroundRepeat:"no-repeat",
     
 }

}));


function Head3() {
  const classes=useStyles();
  return (

    <div className="App">
 

<Box  style={{backgroundImage:`url(${image})`}} className={classes.hero}>
       <Container>
       <Box pt={2}>
          
           <Typography align="center" variant="h5"> Hasty and Nasty</Typography>
           <Box >
           <Typography align="center"> The only thing we're serious about is food</Typography>  
           </Box>
           <Box mt={6} >
               <Grid container spacing={4} justify="center">
                   <Grid item> Home</Grid>
                   <Grid item> About</Grid>
                   <Grid item>Menu</Grid>
                   <Grid item> Terms and Conditions</Grid>
                   <Grid item> Contact</Grid>



               </Grid>



           </Box>

          <Box display="flex" justifyContent="space-between" width="90%">
              
                  <Box ml={12}>    
                      <Grid container spacing={3}>
                          <Grid item   >  <FacebookIcon />   </Grid>
                          <Grid item   >  <TwitterIcon />   </Grid>
                          <Grid item   >  <InstagramIcon />   </Grid>
                           </Grid>
                  </Box>
                     
                  <Box ml={12}>    
                      <Button variant="outlined" style={{color:"white"}}> Order Online</Button>
                  </Box>


          </Box>


       </Box>
               <Box>  
                   <Typography align="center"> &copy; Copyright 2021 |All Right Reserved.</Typography>
               </Box>

        </Container>
      </Box>
  </div>
    
  );
}

export default Head3;
