import React from 'react'
import { Avatar,Grid ,Card, CardActionArea, CardActions, CardContent,CardMedia ,AppBar, Toolbar, Typography,Box,Container  } from '@material-ui/core';
import  { makeStyles } from '@material-ui/core/styles'
import NoteIcon from '@material-ui/icons/Note';
const useStyles=  makeStyles((theme)=> ({
    root: {
 height:'350px',
 width: '300px'
    },
    Actions: {
        display: "flex",
        margin: " 0 10px",
        justifyContent: "space-between"
    },
    author: {
        display:"flex"
    }
  }));
  
const Cards7 = () => {
    const classes=useStyles();

    return (
        <div>
            
            
               <Card className={classes.root}>
      <CardActionArea>
        <CardMedia component="img" height="140" image="https://images.unsplash.com/photo-1526498460520-4c246339dccb?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"
          title="Contemplative Reptile"
        />
        <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
            Stephen
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
          Stephen Edwin King was born the second son of Donald and Nellie Ruth Pillsbury King. After his father 
          left them when Stephen was two years old. 
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions className={classes.Actions} >
          <Box className={classes.author}>
              <Avatar src="https://images.gr-assets.com/authors/1362814142p5/3389.jpg">

              </Avatar>
              <Box ml={2}>
                  <Typography variant="subtitle2"> Stephen King </Typography>
                  <Typography variant="subtitle2"> Jan 27, 2016 </Typography>

              </Box>
               </Box>  
               <Box>
               <NoteIcon />
              </Box>
             

      </CardActions>
    </Card>
                        
        </div>
    )
}

export default Cards7;
