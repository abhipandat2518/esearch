import React,{useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { CssBaseline,Menu,MenuItem,Grid,Box } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
//   title: {
//     flexGrow: 1,
//   },
}));

export default function Nav() {
  const classes = useStyles();
  const[open,setOpen]=useState(null);
  const handleopen=(e)=>setOpen(e.currentTarget);
  const handleClose=()=>setOpen(false);

  return (
    <div className={classes.root}>
      <AppBar position="static" style={{backgroundColor:"#252525"}}>
        <Toolbar>
           <Box  width="100%" display="flex" direction="row" justifyContent="space-between">
           
               <Box ml={8}>
                    <Grid container direction="column">  
                    <Grid item>     
                    <Typography align="center" variant="h4"> Hasty and Nasty</Typography>
                    
                      </Grid>
                      <Grid item>     
                    <Typography> The only thing we're serious about is food</Typography>
                    
                      </Grid>
                    
                       </Grid>
                        </Box>
               
               
               
               <Box>
                 <Grid container spacing={3}> 
                 <Grid item>     
                    <Typography> Home</Typography>
                    
                      </Grid>

                      <Grid item>     
                    <Typography> Menu</Typography>
                    
                      </Grid>
                      <Grid item>     
                    <Typography> About</Typography>
                    
                      </Grid>
                      <Grid item>     
                    <Typography> News</Typography>
                    
                      </Grid>
                      <Grid item>     
                    <Typography> Contact</Typography>
                    
                      </Grid>
                      <Grid item>     
                    <Typography> Reservation</Typography>
                    
                      </Grid>
                 </Grid>
               </Box>



           </Box>
        </Toolbar>
      </AppBar>
    </div>
  );
}