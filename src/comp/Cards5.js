import React from 'react'
import { Avatar,Grid ,Card, CardActionArea, CardActions, CardContent,CardMedia ,AppBar, Toolbar, Typography,Box,Container  } from '@material-ui/core';
import  { makeStyles } from '@material-ui/core/styles'
import BookmarkBorderIcon from '@material-ui/icons/BookmarkBorder';
const useStyles=  makeStyles((theme)=> ({
    root: {
 height:'350px',
 width: '300px'
    },
    Actions: {
        display: "flex",
        margin: " 0 10px",
        justifyContent: "space-between"
    },
    author: {
        display:"flex"
    }
  }));
  
const Cards5 = () => {
    const classes=useStyles();

    return (
        <div>
            
            
               <Card className={classes.root}>
      <CardActionArea>
        <CardMedia component="img" height="140" image="https://images.unsplash.com/photo-1544982877-f0f4427dee24?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80"
          title="Contemplative Reptile"
        />
        <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
            Ernist
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            On July 21, 1899, Ernest Miller Hemingway, author of such novels as “For Whom the Bell Tolls” and “The Old Man and the Sea,” is born in Oak Park, Illinois.           </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions className={classes.Actions} >
          <Box className={classes.author}>
              <Avatar src="https://www.thoughtco.com/thmb/NzCsI-A70Qp1Q68FR2tMfLXwcGc=/3112x3112/smart/filters:no_upscale()/ernest-hemingway-514896254-5c91ba79c9e77c0001e11e36.jpg">

              </Avatar>
              <Box ml={2}>
                  <Typography variant="subtitle2"> Ernist Hemingway </Typography>
                  <Typography variant="subtitle2"> May 14, 2010 </Typography>

              </Box>
               </Box>  
               <Box>
               <BookmarkBorderIcon />
              </Box>
             

      </CardActions>
    </Card>
                        
        </div>
    )
}

export default Cards5;
