import React,{useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { CssBaseline,Menu,MenuItem,Grid,Box,Container } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
//   title: {
//     flexGrow: 1,
//   },
}));

export default function Head2() {
  const classes = useStyles();


  return (
<Container>
      <Box align="center" mt={4}>     
          <Typography variant="h3" style={{fontWeight:"800"}}>   Discover  </Typography>
          <Typography variant="h3"> Most Famous Restaurant</Typography>
          <Typography variant="h4"> ......</Typography>

      </Box>
</Container>
  );
}