import React from 'react'
import { Typography,Container,Box,Grid,Button } from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles';
import { BrowserRouter, Route, Link, Switch } from "react-router-dom";

import image from "../bg.png"
import image1 from "../blog-img-1.jpg"
const useStyles=  makeStyles((theme)=> ({
  root: {
   backgroundColor:"#fff"
  },
  hero:{
    height: "500px",
    backgroundPosition: "center",
    backgroundRepeat:"no-repeat",
    backgroundSize:"cover",
     position:"relative",
    displa:"flex",
    justifyContent:"center",
    alignItems:"center",
    color:"#fff",
    fontSize:"4rem",
    [theme.breakpoints.down("sm")]:{
      height:300,
      fontSize:"2em"
    }
},
 img:{
     height:400,
     backgroundRepeat:"no-repeat",
     
 }

}));


function Head3() {
  const classes=useStyles();
  return (

    <div className="App">
 

<Box style={{backgroundImage:`url(${image})`}} className={classes.hero}>
       <Container>
       <Box display="flex"  > 
        <Box mt={2} style={{width:"60%"}}> 
         <Typography > Our Story  </Typography>

         <Typography variant="h4"> Few words about us</Typography>
         <Typography variant="h4"> .....</Typography>
         <Typography variant="body1"> Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br/>
          Lorem Ipsum has been the industry's standard dummy text ever since the <br/>
          1500s, when an unknown printer took a galley of type and scrambled it to <br/>
           make a type specimen book. It has survived not only five centuries, but also <br/>
            th essentially unchanged. Lorem Ipsum is simply dummy text of the printing<br/>
             and typesetting industry.

</Typography>


         <Box mt={4}>  <Button variant="outlined" style={{color:"white"}}> Learn more about </Button> </Box>

        </Box>


      <Box style={{backgroundImage:`url(${image1})`,width:"60%"}} className={classes.img} mt={8}>    </Box>

        </Box>
        </Container>
      </Box>
  </div>
    
  );
}

export default Head3;
