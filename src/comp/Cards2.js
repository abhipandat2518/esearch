import React from 'react'
import { Avatar,Grid ,Card, CardActionArea, CardActions, CardContent,CardMedia ,AppBar, Toolbar, Typography,Box,Container  } from '@material-ui/core';
import  { makeStyles } from '@material-ui/core/styles'
import AdbIcon from '@material-ui/icons/Adb';
const useStyles=  makeStyles((theme)=> ({
    root: {
 height:'350px',
 width: '300px'
    },
    Actions: {
        display: "flex",
        margin: " 0 10px",
        justifyContent: "space-between"
    },
    author: {
        display:"flex"
    }
  }));
  
const Cards2 = () => {
    const classes=useStyles();

    return (
        <div>
            
            
               <Card className={classes.root}>
      <CardActionArea>
        <CardMedia component="img" height="140" image="https://images.unsplash.com/photo-1576936422505-18d321d54d40?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80"
          title="Contemplative Reptile"
        />
        <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
            Rowling
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
          Now J. K. Rowling is richer than the Queen, according to the Sunday Times Rich List. The creator of Harry Potter, who wrote her first children's book. 
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions className={classes.Actions} >
          <Box className={classes.author}>
              <Avatar src="https://www.biography.com/.image/ar_1:1%2Cc_fill%2Ccs_srgb%2Cfl_progressive%2Cq_auto:good%2Cw_1200/MTc5OTUwMTA3MzE4Mjk3OTQ0/gettyimages-1061157246.jpg">

              </Avatar>
              <Box ml={2}>
                  <Typography variant="subtitle2"> JK Rowling </Typography>
                  <Typography variant="subtitle2"> May 19, 2012 </Typography>

              </Box>
               </Box>  
               <Box>
               <AdbIcon />
              </Box>
             

      </CardActions>
    </Card>
                        
        </div>
    )
}

export default Cards2;
