import React from 'react'
import { Avatar,Grid ,Card, CardActionArea, CardActions, CardContent,CardMedia ,AppBar, Toolbar, Typography,Box,Container  } from '@material-ui/core';
import  { makeStyles } from '@material-ui/core/styles'
import AllInclusiveIcon from '@material-ui/icons/AllInclusive';
const useStyles=  makeStyles((theme)=> ({
    root: {
 height:'350px',
 width: '300px'
    },
    Actions: {
        display: "flex",
        margin: " 0 10px",
        justifyContent: "space-between"
    },
    author: {
        display:"flex"
    }
  }));
  
const Cards8 = () => {
    const classes=useStyles();

    return (
        <div>
            
            
               <Card className={classes.root}>
      <CardActionArea>
        <CardMedia component="img" height="140" image="https://images.unsplash.com/photo-1506526794364-ba711a0d97fc?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NDl8fHJlYWN0JTIwanN8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
          title="Contemplative Reptile"
        />
        <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
            Elon
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
          Elon Musk is working to revolutionize transportation both on Earth, through electric car maker Tesla - and in space, via rocket producer SpaceX. 
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions className={classes.Actions} >
          <Box className={classes.author}>
              <Avatar src=" https://h7f7z2r7.stackpathcdn.com/sites/default/files/images/articles/musk-wealth-1200.jpg">

              </Avatar>
              <Box ml={2}>
                  <Typography variant="subtitle2"> Elon Musk </Typography>
                  <Typography variant="subtitle2"> oct 25, 2019 </Typography>

              </Box>
               </Box>  
               <Box>
               <AllInclusiveIcon />
              </Box>
             

      </CardActions>
    </Card>
                        
        </div>
    )
}

export default Cards8;
